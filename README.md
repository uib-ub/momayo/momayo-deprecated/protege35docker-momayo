# Protege 3.5 Dockerfile

## To run

'docker build .'
'docker run -p5100:5100 -p5200:5200' hash from build...

## Info

Test users have been added to 'projects/server/metaproject.pprj'

To edit the metaproject, open the pprj on 'metaproject.pprj' with Protege-3.5 client.
An empty marcus-bs project (no data, pprj is copied as is) is available from '/projects/marcus-bs/marcus-bs.pprj'. This will be deployed and can be used to test out and experiment with widgets etc. On deploying to server a separate file with data is fetched, while the pprj from this git repo is used.

Connect to localhost:5100 in Protege.
with user marcus:marcus

## Dockerfiles
**protege35/base** is  aProtege 3.5 server without data. Contains metaproject but no 
actual projects, which means starting the image gives a warning. Can be used as a base image or with a volume for `/data/protege-projects/...`

**protege35/public** extends server with data dump from Open sparql endpoint at sparql.ub.uib.no 
Public extends 

**protege35/svn-vol** Data Volume only which checks out newest svn info.

**protege35/update** Runs ant job for updating protege35 data from volume.






docker container stop server

docker run update

docker start server

docker server > update

## Related

(https://github.com/ubbdst/protege-pprj)
