<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="3.0">
    <xsl:param name="lookup" select="'test/skeivt-arkiv-test.pprj'"/>
    
    <xsl:variable name="item-firstline" select="'^\(\[([^\]]+)\].+'"/>
    <xsl:variable name="item-content" select="'(\n\n	[\s\S]+?\n\n)'"/>
    <xsl:variable name="item-no-content" select="'(\n\)\n\n)'"/>
    <xsl:variable name="item-regex" select="concat($item-firstline,'(',$item-content,'|',$item-no-content,')')">
    
    
        <!--GetPropertyListFromMain returns a list of Propertys based on name class-->
          <!--  (name "http://purl.org/ontology/bibo/Document") -->
        
        <!--GetPropertyByName-->
        <!--propertyHasWidget-->
        <!-- classHasForm-->
        
      <!--  RemoveFromProperty(name,regex)-->
        
        

    </xsl:variable>
    <xsl:template match="/">
        <!-- add to y axis-->
        <xsl:analyze-string select="unparsed-text-lines(doc($lookup))" regex="\(	y\s([0-9]+)\)">
            <xsl:matching-substring>
                <xsl:text>(y </xsl:text><xsl:value-of select="xs:integer(regex-group(1))+100"/>
            </xsl:matching-substring>
            <xsl:non-matching-substring><xsl:value-of select="."/></xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>
    
</xsl:stylesheet>