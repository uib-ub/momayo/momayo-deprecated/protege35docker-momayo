#!/bin/bash
function gracefulshutdown {
  echo "Shutting down Protege Server"
  exec "/opt/Protege-3.5/shutdown_protege_server.sh"
}
trap gracefulshutdown SIGTERM
trap gracefulshutdown SIGINT

exec "/opt/Protege-3.5/run_protege_server.sh" &
wait